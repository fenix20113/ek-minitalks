<?php

namespace App\Controller;

use App\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ItemsController extends AbstractController
{
    /**
     * @Route("/items", name="items", methods={"GET"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {

        $itemRepository = $this->getDoctrine()->getRepository(Item::class);
        $userQuery = trim($request->query->get('user'));

        $params = [];
        if ($userQuery) {
            $params['user'] = $userQuery;
        }

        return $this->json(
            [
                'items' => $itemRepository->findBy($params),
            ]
        );
    }

    /**
     * @Route("/items", name="create_item", methods={"POST"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Symfony\Component\Serializer\SerializerInterface $serializer
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function createItem(
        Request $request,
        SerializerInterface $serializer
    ): JsonResponse {
        $em = $this->getDoctrine()->getManager();

        $item = $serializer->deserialize(
            $request->getContent(),
            Item::class,
            'json'
        );

        $em->persist($item);
        $em->flush();

        return $this->json($item);

    }

    /**
     * @Route("/items/{id}", name="delete_item", methods={"DELETE"}, requirements={"id":"\d+"})
     *
     * @param \App\Entity\Item $item
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteItem($id): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $itemRepository = $this->getDoctrine()->getRepository(Item::class);
        $item = $itemRepository->find($id);
        if (!$item) {
            throw new NotFoundHttpException();
        }
        $em->remove($item);
        $em->flush();

        return $this->json('ok');
    }

}
