# ek-minitalks

    wget https://get.symfony.com/cli/installer -O - | bash
    symfony new my_project

    echo "7.3" > .php-version
 
    symfony server:start

    composer require orm 
    composer require doctrine/annotations
    composer require serializer         

https://symfony.com/doc/current/bundles/SymfonyMakerBundle/index.html

    composer require symfony/maker-bundle --dev
 
    bin/console doctrine:database:create

## Generate enity

    bin/console make:migration
    bin/console doctrine:migrations:migrate

## Run migrations
    
    docker-compose run api bin/console doctrine:migrations:migrate
