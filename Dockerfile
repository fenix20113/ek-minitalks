FROM composer:latest as dependencies

FROM php:7.3-apache

COPY --from=dependencies /usr/bin/composer /usr/bin/composer

RUN rm -f /etc/apache2/sites-enabled/*
COPY ./docker/minitalks.conf /etc/apache2/sites-enabled/minitalks.conf

RUN apt-get update \
    && apt-get install git-core -y \
    && apt-get install -y zip libzip-dev \
    && apt-get install wget -y

RUN a2enmod rewrite \
    && service apache2 restart

RUN docker-php-ext-install pdo_mysql

RUN wget https://get.symfony.com/cli/installer -O - | bash \
    &&  mv /root/.symfony/bin/symfony /usr/local/bin/symfony

WORKDIR /var/www/html/ek_api